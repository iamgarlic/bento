package utils

import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

import scala.concurrent.Future

import models._
import models.serialization._

import play.api.libs.json._

object MenuFileProvider {
	val getMenu: Future[String] = Future {
		val response = scala.io.Source.fromFile("menu.json", "UTF-8").getLines.map(x=>x.trim).mkString
		response
	}

	val getMenuObj =
		Json.parse(scala.io.Source.fromFile("menu.json", "UTF-8").getLines.map(x=>x.trim).mkString).validate[List[Menu]] match {
			case JsSuccess(m, p) => m
      case JsError(err) => {
        throw new Exception("invalid request: %s".format(err.toString))
      }
		}
		
}
