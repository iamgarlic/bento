package controllers

import play.api._
import play.api.mvc._

import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.ws._
import play.api.libs.iteratee.Enumerator
// import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.util.{Success, Failure}
import play.api.libs.json._
import reactivemongo.api._

// Reactive Mongo plugin, including the JSON-specialized collection
import play.modules.reactivemongo._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection

import reactivemongo.core.commands.LastError

import play.api.Logger

import models._
import models.serialization._

import utils._

object BentoDBController extends Controller with MongoController {

	def generateUUID = java.util.UUID.randomUUID.toString
	def now = (System.currentTimeMillis/1000).toInt

	lazy val ordersCollection = current.configuration.getString("mongodb.collection.orders").getOrElse("orders")
  def orders: JSONCollection = ReactiveMongoPlugin.db.collection[JSONCollection](ordersCollection)

  def index = Action {
    Ok(views.html.index("Your new application is ready.")).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
  }


  def menu(`type`: Option[String], location: Option[String], name: Option[String]) = Action.async {
  	for (m <- MenuFileProvider.getMenu)
  	yield {
  		println(m)
  		Ok(Json.parse(m)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
  	}
  }

  def createGroupOrder(creatorId: String, creatorName: String, shopId: String, groupId: String) = Action.async {

    val shopName = MenuFileProvider.getMenuObj.filter(m=>m.shopId.compareTo(shopId)==0) match {
      case l: List[Menu] if l.size > 0 => l(0).name
      case _ => throw new Exception("invalid shopId")
    }
    val groupOrder = GroupOrder(Some(generateUUID), shopId, Some(shopName), "open", Some(now), Some(creatorId), Some(creatorName), Some(groupId), None, None)
  	val orderDetail = Json.toJson(groupOrder)    
  	for{    
      f <- {                
        val detail = Json.toJson(groupOrder)
        orders.insert(groupOrder)
      }
    }
  	yield {
  		Ok(Json.toJson(orderDetail)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
  	}
  }

  def queryGroupOrder(orderId: String, aggregateBy: String = "member") = Action.async {  	
  	val fgl: Future[List[GroupOrder]] = orders.find(Json.obj("orderId" -> orderId)).cursor[GroupOrder].collect[List]()
  	for (gl <- fgl)
  	yield {
  		Ok(Json.toJson(gl))
  	}
  }

  def updateGroupOrder(orderId: String, creatorId: String, status: String) = Action.async {

  	// TODO: add creator check
  	// TODO: disable status update if status is cancel or closed  	
  	val update = s"""{"$$set":{"status":"closed"}}""" 
  	for(f <- orders.update(Json.obj("orderId" -> orderId), Json.parse(update)))
  	yield {
  		Ok(Json.obj("orderId" -> orderId, "ok"->f.ok)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
  	}
  }

  def updateOrder(orderId: String, memberId: String, name: String) = Action.async(parse.json) { request =>
  	
  	val items = request.body.validate[List[Item]] match {
      case JsSuccess(m, p) => m
      case JsError(err) => {
        Logger.error(s"request.body: ${request.body}")
        throw new Exception("invalid request: %s".format(err.toString))
    	}
    }

    val member = Member(mid=memberId, name=name, items=Some(items))
    // val update = s"""{"$$addToSet":{"participants":{"uid":"$uid","status":1}}}"""
    val fgl: Future[List[GroupOrder]] = orders.find(Json.obj("orderId" -> orderId)).cursor[GroupOrder].collect[List]()
    for {
    	gl <- fgl
    	updateResult <- {
    		gl.size match {
    			case 1 => 
    				val go = gl(0)
    				val target = go.members.getOrElse(List())filter(m=>m.mid.compareTo(memberId)==0)    				
    				target.size match {
    					case 0 => // add member to set
    						val update = s"""{"$$addToSet":{"members":${Json.toJson(member).toString}}}"""
    						println(s"add member to set: %s".format(update))
    						orders.update(Json.obj("orderId" -> orderId), Json.parse(update))    						
    					case 1 => // update existing member order
                val update = s"""{"$$set":{"members.$$":${Json.toJson(member).toString}}}"""
                orders.update(Json.obj("orderId" -> orderId, "members.mId" -> memberId), Json.parse(update))
    				}
    			case 0 => 
    				val err = new LastError(ok = false, err = Option("order not found"), None, None, None, 0, false) 
    				Future{ err }
    		}
    	}
    }
    yield {
    	if(updateResult.ok) {
    		Ok(Json.obj("orderId" -> orderId, "ok" -> updateResult.ok)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
    	}
    	else{
    		BadRequest(Json.obj("orderId" -> orderId, "ok" -> updateResult.ok, "err" -> updateResult.err.get)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
    	} 
    }
  }

  def queryHistoryGroupOrders(groupId: String, creatorId: Option[String], status: Option[String]) = Action.async {
  	
  	// TODO: add status and creatorId filter
    val query = status.getOrElse("all") match {
      case "open" => Json.obj("groupId" -> groupId, "status" -> "open")
      case _ => Json.obj("groupId" -> groupId)
    }
  	val fgl: Future[List[GroupOrder]] = orders.find(query).cursor[GroupOrder].collect[List]()
  	for (gl <- fgl)
  	yield {
      val sortedOrder = gl.sortWith(_.createTime.getOrElse(0) > _.createTime.getOrElse(0))
      sortedOrder.foreach{ go =>        
        go.items = MenuFileProvider.getMenuObj.filter(m=>m.shopId.compareTo(go.shopId)==0) match {
          case l: List[Menu] if l.size > 0 => l(0).items  
          case _ => None      
        }  
      }

  		Ok(Json.toJson(sortedOrder)).withHeaders("Access-Control-Allow-Origin" -> "*", "Access-Control-Allow-Methods" -> "POST, GET, OPTIONS")
  	}
  }

}