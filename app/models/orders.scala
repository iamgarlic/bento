package models

import play.api.libs.json._ // JSON library
import play.api.libs.json.Reads._ // Custom validation helpers
import play.api.libs.functional.syntax._ // Combinator syntax

case class Member(
  mid: String,
  name: String,
  items: Option[List[Item]]
)

case class GroupOrder(
  orderId: Option[String],
  shopId: String,
  shopName: Option[String],
  status: String,
  createTime: Option[Int],
  creatorId: Option[String],
  creatorName: Option[String],
  groupId: Option[String],
  var items: Option[List[Item]] = None,
  members: Option[List[Member]]
)

case class GroupOrderUpdate(
  var orderId: Option[String],
  var shopId: Option[String],
  var shopName: Option[String],
  var status: Option[String],
  var createTime: Option[Int],
  var creatorId: Option[String],
  var groupId: Option[String],
  var members: Option[List[Member]]
)

