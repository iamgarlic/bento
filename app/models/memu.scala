package models

import play.api.libs.json._ // JSON library
import play.api.libs.json.Reads._ // Custom validation helpers
import play.api.libs.functional.syntax._ // Combinator syntax

case class Menu(
  shopId: String,
  name: String,
  `type`: Option[String],
  location: Option[String],
  items: Option[List[Item]]
)

case class Item(
  itemId: String,
  name: String,
  number: Option[Int],
  price: Double
)

case class BsonTime(timestamp: String)

object serialization {

  implicit val itemWrites: Writes[Item] = (
    (JsPath \ "itemId").write[String] and 
    (JsPath \ "name").write[String] and 
    (JsPath \ "number").writeNullable[Int] and
    (JsPath \ "price").write[Double]
  )(unlift(Item.unapply))

  implicit val menuWrites: Writes[Menu] = (
    (JsPath \ "shopId").write[String] and 
    (JsPath \ "name").write[String] and 
    (JsPath \ "type").writeNullable[String] and
    (JsPath \ "location").writeNullable[String] and
    (JsPath \ "items").writeNullable[List[Item]]
  )(unlift(Menu.unapply))

  implicit val memberWrites: Writes[Member] = (
    (JsPath \ "mId").write[String] and 
    (JsPath \ "name").write[String] and 
    (JsPath \ "items").writeNullable[List[Item]]
  )(unlift(Member.unapply))

  implicit val groupOrderWrites: Writes[GroupOrder] = (
    (JsPath \ "orderId").writeNullable[String] and 
    (JsPath \ "shopId").write[String] and 
    (JsPath \ "shopName").writeNullable[String] and 
    (JsPath \ "status").write[String] and
    (JsPath \ "createTime").writeNullable[Int] and
    (JsPath \ "creatorId").writeNullable[String] and 
    (JsPath \ "creatorName").writeNullable[String] and 
    (JsPath \ "groupId").writeNullable[String] and 
    (JsPath \ "items").writeNullable[List[Item]] and
    (JsPath \ "members").writeNullable[List[Member]]
  )(unlift(GroupOrder.unapply))

  implicit val groupOrderUpdateWrites: Writes[GroupOrderUpdate] = (
    (JsPath \ "orderId").writeNullable[String] and 
    (JsPath \ "shopId").writeNullable[String] and 
    (JsPath \ "shopName").writeNullable[String] and 
    (JsPath \ "status").writeNullable[String] and
    (JsPath \ "createTime").writeNullable[Int] and
    (JsPath \ "creatorId").writeNullable[String] and 
    (JsPath \ "groupId").writeNullable[String] and 
    (JsPath \ "members").writeNullable[List[Member]]
  )(unlift(GroupOrderUpdate.unapply))

  implicit val itemReads: Reads[Item] = (
    (JsPath \ "itemId").read[String] and
    (JsPath \ "name").read[String] and
    (JsPath \ "number").readNullable[Int] and
    (JsPath \ "price").read[Double]
  )(Item.apply _)

  implicit val menuReads: Reads[Menu] = (
    (JsPath \ "shopId").read[String] and
    (JsPath \ "name").read[String] and
    (JsPath \ "type").readNullable[String] and
    (JsPath \ "location").readNullable[String] and
    (JsPath \ "items").readNullable[List[Item]]
  )(Menu.apply _)

  implicit val memberReads: Reads[Member] = (
    (JsPath \ "mId").read[String] and
    (JsPath \ "name").read[String] and
    (JsPath \ "items").readNullable[List[Item]]
  )(Member.apply _)

  implicit val groupOrderReads: Reads[GroupOrder] = (
    (JsPath \ "orderId").readNullable[String] and
    (JsPath \ "shopId").read[String] and
    (JsPath \ "shopName").readNullable[String] and
    (JsPath \ "status").read[String] and
    (JsPath \ "createTime").readNullable[Int] and
    (JsPath \ "creatorId").readNullable[String] and
    (JsPath \ "creatorName").readNullable[String] and
    (JsPath \ "groupId").readNullable[String] and
    (JsPath \ "items").readNullable[List[Item]] and
    (JsPath \ "members").readNullable[List[Member]]
  )(GroupOrder.apply _)

  implicit val groupOrderUpdateReads: Reads[GroupOrderUpdate] = (
    (JsPath \ "orderId").readNullable[String] and
    (JsPath \ "shopId").readNullable[String] and
    (JsPath \ "shopName").readNullable[String] and
    (JsPath \ "status").readNullable[String] and
    (JsPath \ "createTime").readNullable[Int] and
    (JsPath \ "creatorId").readNullable[String] and
    (JsPath \ "groupId").readNullable[String] and
    (JsPath \ "members").readNullable[List[Member]]
  )(GroupOrderUpdate.apply _)

  implicit val bsonTimeReads: Reads[BsonTime] =
    (JsPath \ "$date").read[String].map(v => BsonTime(v))

  implicit val bsonTimeWrites: Writes[BsonTime] =
    (JsPath \ "$date").write[String].contramap(_.timestamp)
}