package models.error

case class ClientException() extends Exception 
case class ServerException() extends Exception