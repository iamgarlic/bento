import play.api._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.libs.ws._
import play.api.libs.iteratee.Enumerator

import scala.concurrent.Future
import scala.util.{Success, Failure}

import models.error._


object AccessLoggingFilter extends Filter {
  
  def apply(next: (RequestHeader) => Future[Result])(request: RequestHeader): Future[Result] = {
    val resultFuture = next(request)
    
    resultFuture.foreach(result => {
      val msg = s"method=${request.method} uri=${request.uri}" + // "remote-address=${request.remoteAddress}" +
        s" status=${result.header.status}";
      println(msg)
    })
    
    resultFuture
  }
}

object Global extends WithFilters(AccessLoggingFilter) {
  override def onStart(app: Application) {
    Logger.info("Application has started")
  }

  override def onError(request: RequestHeader, ex: Throwable): Future[Result] = {
    ex match {
      case ec: ClientException => Future.successful(BadRequest(ex.getMessage))
      case es: ServerException => Future.successful(InternalServerError(ex.getMessage))
      case _ => Future.successful(InternalServerError(ex.getMessage))
    } 
  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
  }

}